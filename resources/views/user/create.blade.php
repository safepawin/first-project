@extends('layouts.app')

@section('content')
    <div class="form-group row">
        <label for="firstname_en" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อ ภาษาอังกฤษ') }}</label>

        <div class="col-md-6">
            <input id="firstname_en" type="text" class="form-control @error('firstname_en') is-invalid @enderror"
                name="firstname_en" value="{{ old('firstname_en') }}" required autocomplete="firstname_en" autofocus>

            @error('firstname_en')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="lastname_en" class="col-md-4 col-form-label text-md-right">{{ __('นามสกุล ภาษาอังกฤษ') }}</label>

        <div class="col-md-6">
            <input id="lastname_en" type="text" class="form-control @error('lastname_en') is-invalid @enderror"
                name="lastname_en" value="{{ old('lastname_en') }}" required autocomplete="lastname_en" autofocus>

            @error('lastname_en')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('เพศ') }}</label>

        <div class="col-md-6">
            <select id="gender" type="text" class="form-control @error('gender') is-invalid @enderror" name="gender"
                value="{{ old('gender') }}" required autocomplete="gender" autofocus>
                <option value="0">ชาย</option>
                <option value="1">หญิง</option>
            </select>

            @error('gender')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('ที่อยู่') }}</label>
        <div class="col-md-6">
            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                value="{{ old('address') }}" required autocomplete="address" autofocus>
            @error('address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="tel" class="col-md-4 col-form-label text-md-right">{{ __('เบอร์โทร') }}</label>
        <div class="col-md-6">
            <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel"
                value="{{ old('tel') }}" required autocomplete="tel" autofocus minlength="10" maxlength="10">
            @error('tel')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="identity" class="col-md-4 col-form-label text-md-right">{{ __('เลขบัตรชระชาชน') }}</label>
        <div class="col-md-6">
            <input id="identity" type="text" class="form-control @error('identity') is-invalid @enderror" name="identity"
                value="{{ old('identity') }}" required autocomplete="identity" autofocus minlength="13" maxlength="13">
            @error('identity')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="salary" class="col-md-4 col-form-label text-md-right">{{ __('เงินเดือน') }}</label>
        <div class="col-md-6">
            <input id="salary" type="number" class="form-control @error('salary') is-invalid @enderror" name="salary"
                value="{{ old('salary') }}" required autocomplete="salary" autofocus>
            @error('salary')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
@endsection
