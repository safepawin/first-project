@extends('admin.layout')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit</h1>
            {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
        </div>

        <!-- Content Row -->

        <div class="row">

            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Members</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <form class="col-md-12 row" action="{{ route('admin.update',$member->id) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="firstname_th">ชื่อ ภาษาไทย</label>
                                        <input type="text" name="firstname_th" class="form-control" placeholder="สมชาย" value="{{ $member->firstname_th }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="lastname_th">นามสกุล ภาษาไทย</label>
                                        <input type="text" name="lastname_th" class="form-control" placeholder="สายชน" value="{{ $member->lastname_th }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="firstname_en">ชื่อ ภาษาอังกฤษ</label>
                                        <input type="text" name="firstname_en" class="form-control" placeholder="john xxx" value="{{ $member->firstname_en }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="lastname_en">นามสกุล ภาษาอังกฤษ</label>
                                        <input type="text" name="lastname_en" class="form-control" placeholder="xxx value" value="{{ $member->lastname_en }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <label for="gender">เพศ</label>
                                        <select class="form-control" type="text" name="gender" placeholder="เพศ" required>
                                            <option value="1" {{ $member->gender == 1 ? 'selected' : ''}}>ชาย</option>
                                            <option value="2" {{ $member->gender == 2 ? 'selected' : ''}}>หญิง</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="tel">เบอร์ติดต่อ</label>
                                        <input class="form-control" type="text" name="tel" placeholder="tel" required minlength="7" value="{{ $member->tel }}"
                                            maxlength="10" />
                                    </div>
                                </div>
                                <div class="col-md-5 col-lg-6">
                                    <div class="form-group">
                                        <label for="tel">E-mail</label>
                                        <input type="email" name="email" class="form-control" placeholder="email" value="{{ $member->email }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="address">ที่อยู่</label>
                                        <input type="text" name="address" class="form-control" placeholder="address" value="{{ $member->address }}"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="identity">เลขบัตรประชาชน</label>
                                        <input type="text" name="identity" class="form-control" placeholder="identity" value="{{ $member->identity }}"
                                            required minlength="13" maxlength="13" />
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <label for="salary">เงินเดือน</label>
                                        <input type="number" name="salary" class="form-control" placeholder="salary" value="{{ $member->salary }}"
                                            required minlength="20" />
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <button class="btn btn-primary">
                                        ส่งข้อมูล
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
