@extends('admin.layout')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
        </div>

        <!-- Content Row -->

        <div class="row">

            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Members</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center">
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Tel.</th>
                                        <th>email</th>
                                        <th>เลขบัตรประชาชน</th>
                                        <th>Salary</th>
                                        <th>ภาษี</th>
                                        <th>ผู้แก้ไข</th>
                                        <th>เวลา</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($logs as $item)
                                        <tr class="text-center">
                                            <td>{{ $item->firstname_th }}</td>
                                            <td>{{ $item->lastname_th }}</td>
                                            <td>{{ $item->firstname_en }}</td>
                                            <td>{{ $item->lastname_en }}</td>
                                            <td>{{ $item->gender == 1 ? 'Male' : 'Female' }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td>{{ $item->tel }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->identity }}</td>
                                            <td>{{ $item->salary }}</td>
                                            <td>{{ $item->salary / 10 }}</td>
                                            <td>{{ $item->user->name }}</td>
                                            <td>{{ $item->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
