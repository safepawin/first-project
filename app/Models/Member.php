<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $fillable = [
        'firstname_th',
        'lastname_th',
        'firstname_en',
        'lastname_en',
        'gender',
        'address',
        'tel',
        'identity',
        'salary',
        'email'
    ];

    public function memberLog(){
        return $this->hasMany(MemberLog::class);
    }

    public function calculateTax($salary){
        $result = 0;
        if($salary >= 1 && $salary <= 150000){
            $result = 0;
        }elseif($salary >= 150001 && $salary <= 300000){
            $result =$salary * 5/100;
        }elseif($salary >= 300001 && $salary <= 500000){
            $result =$salary * 10/100;
        }elseif($salary >= 500001 && $salary <= 750000){
            $result =$salary * 15/100;
        }elseif($salary >= 750001 && $salary <= 1000000){
            $result =$salary * 20/100;
        }elseif($salary >= 1000001 && $salary <= 2000000){
            $result =$salary * 25/100;
        }elseif($salary >= 2000001 && $salary <= 5000000){
            $result =$salary * 30/100;
        }elseif($salary >= 5000001){
            $result =$salary * 35/100;
        }else{
            $result = 0;
        }
        return $result;
    }
}
