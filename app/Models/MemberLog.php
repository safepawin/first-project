<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberLog extends Model
{
    use HasFactory;
    protected $fillable = ['firstname_th', 'lastname_th', 'firstname_en', 'lastname_en', 'gender', 'address', 'tel', 'identity', 'salary', 'email', 'member_id', 'user_id', 'status'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
