<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Models\MemberLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::simplePaginate(5)->withQueryString();
        return view('admin.index')->with('members', $members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        return view('admin.edit')->with('member', $member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|max:255',
            'firstname_en' => 'required|max:255',
            'lastname_en' => 'required|max:255',
            'firstname_th' => 'required|max:255',
            'lastname_th' => 'required|max:255',
            'gender' => 'required|max:255',
            'tel' => 'required|max:10',
            'address' => 'required|max:255',
            'identity' => 'required|max:13',
            'salary' => 'required|max:20',
        ]);
        $member = Member::find($id)->update([
            'email' => $request->email,
            'firstname_en' => $request->firstname_en,
            'lastname_en' => $request->lastname_en,
            'firstname_th' => $request->firstname_th,
            'lastname_th' => $request->lastname_th,
            'gender' => $request->gender,
            'tel' => $request->tel,
            'address' => $request->address,
            'identity' => $request->identity,
            'salary' => $request->salary,
        ]);

        $this->createLog($id, $request, 1);
        return redirect(route('admin.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
        $this->createLog($id, $member, 0);
        $member->delete();
        return redirect(route('admin.index'));
    }

    protected function createLog($id, $member, $status)
    {
        $log = MemberLog::create([
            'user_id' => Auth::user()->id,
            'member_id' => $id,
            'email' => $member->email,
            'firstname_en' => $member->firstname_en,
            'lastname_en' => $member->lastname_en,
            'firstname_th' => $member->firstname_th,
            'lastname_th' => $member->lastname_th,
            'gender' => $member->gender,
            'tel' => $member->tel,
            'address' => $member->address,
            'identity' => $member->identity,
            'salary' => $member->salary,
            'status' => $status
        ]);
    }

    public function memberLog()
    {
        $logs = MemberLog::all();
        return view('admin.memberLog')->with('logs', $logs);
    }

    public function showLog($id)
    {
        $logs = MemberLog::where('member_id', $id)->orderBy('updated_at', 'desc')->get();
        return view('admin.showLog')->with('logs', $logs);
    }

    public function searchData(Request $request)
    {
        $members = Member::where('firstname_th', 'like', '%' . $request->search . '%')
            ->orWhere('lastname_th', 'like', '%' . $request->search . '%')
            ->orWhere('firstname_en', 'like', '%' . $request->search . '%')
            ->orWhere('lastname_en', 'like', '%' . $request->search . '%')
            ->orWhere('identity', 'like', '%' . $request->search . '%')
            ->orWhere('email', 'like', '%' . $request->search . '%')
            ->simplePaginate(5)->withQueryString();
        return view('admin.index')->with('members', $members);
    }
}
