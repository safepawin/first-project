<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class ApiMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255',
            'firstname_en' => 'required|max:255',
            'lastname_en' => 'required|max:255',
            'firstname_th' => 'required|max:255',
            'lastname_th' => 'required|max:255',
            'gender' => 'required|max:255',
            'tel' => 'required|max:10',
            'address' => 'required|max:255',
            'identity' => 'required|max:13',
            'salary' => 'required|max:20',
        ]);

        $user = Member::create([
            'email' => $request->email,
            'firstname_en' => $request->firstname_en,
            'lastname_en' => $request->lastname_en,
            'firstname_th' => $request->firstname_th,
            'lastname_th' => $request->lastname_th,
            'gender' => $request->gender,
            'tel' => $request->tel,
            'address' => $request->address,
            'identity' => $request->identity,
            'salary' => $request->salary
        ]);

        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
