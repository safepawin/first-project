<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_logs', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('member_id');
            $table->string('firstname_th');
            $table->string('lastname_th');
            $table->string('firstname_en');
            $table->string('lastname_en');
            $table->string('gender');
            $table->string('address');
            $table->string('tel');
            $table->string('email');
            $table->string('identity');
            $table->string('salary');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_logs');
    }
}
