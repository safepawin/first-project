<?php

namespace Database\Factories;

use App\Models\Member;
use Illuminate\Database\Eloquent\Factories\Factory;

class MemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'firstname_en' => $this->faker->firstNameMale(),
            'lastname_en' => $this->faker->lastName(),
            'firstname_th' => $this->faker->firstNameMale(),
            'lastname_th' => $this->faker->lastName(),
            'gender' => $this->faker->numberBetween(1,2),
            'tel' => $this->faker->e164PhoneNumber(),
            'address' => $this->faker->address(),
            'identity' => $this->faker->isbn13(),
            'salary' => $this->faker->numberBetween(0,10000000)
        ];
    }
}
